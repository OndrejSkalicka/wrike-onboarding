#!/bin/bash

# exit after any failure
set -e

# don't allow mac to fall to sleep during installation
caffeinate -i -d -w $$ &

if [ $(id -u) = 0 ]; then
   echo "Do NOT run the script as root!" >&2
   exit 1
fi

echo "Running script under $USER."

ARCHITECTURE="$(/usr/bin/uname -m)"

if [[ "${ARCHITECTURE}" == "arm64" ]]
then
  # Apple M1
  CREATE_USER_SCRIPT="/opt/homebrew/bin/createuser"
  HOMEBREW_PREFIX="/opt/homebrew"
else
  # Intel
  CREATE_USER_SCRIPT="/usr/local/opt/postgresql@11/bin/createuser"
  HOMEBREW_PREFIX="/usr/local"
fi

echo "Detected architecture: $ARCHITECTURE"
echo "Using $CREATE_USER_SCRIPT as postgresql createuser script."
echo -en "\007" # beep

read -p "Enter full name (eg. John Doe, no special chars): " fullname

defaultmail="$(echo $fullname | tr '[:upper:]' '[:lower:]' | tr ' ' '.')@team.wrike.com"
read -p "Enter wrike email (default $defaultmail): " email
email=${email:-$defaultmail}

read -p "Enter ABSOLUTE path where you'd like your project stored (default $HOME/IdeaProjects): " projectpath
projectpath=${projectpath:-$HOME/IdeaProjects}

echo "Full name: $fullname"
echo "Email: $email"
echo "Project location: $projectpath"

read -p "Is this correct? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

echo -e "\n\n** INSTALLING BREW **\n\n"

echo -en "\007" # beep
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

echo -e "\n\n** MAKING SURE BREW IS ON PATH **\n\n"

# check from brew install script -- if brew is not on path, put it there
if [[ "$(which brew)" != "${HOMEBREW_PREFIX}/bin/brew" ]]
then
  case "${SHELL}" in
    */bash*)
      if [[ -r "${HOME}/.bash_profile" ]]
      then
        shell_profile="${HOME}/.bash_profile"
      else
        shell_profile="${HOME}/.profile"
      fi
      ;;
    */zsh*)
      shell_profile="${HOME}/.zprofile"
      ;;
    *)
      shell_profile="${HOME}/.profile"
      ;;
  esac

  echo "eval \"\$(${HOMEBREW_PREFIX}/bin/brew shellenv)\"" >> ${shell_profile}
  eval "$(${HOMEBREW_PREFIX}/bin/brew shellenv)"
  echo "Added ${HOMEBREW_PREFIX}/bin/brew to ${shell_profile}"
fi

echo -e "\n\n** INSTALLING DEV TOOLS (git, maven, nginx, ...) **\n\n"

brew install git ant maven nginx mc postgresql@11 rabbitmq gcc redis nano lima
brew install --cask --no-quarantine slack jetbrains-toolbox intellij-idea zoom
# chrome might and might not be installed, better do it with set +e
set +e
brew install --cask --no-quarantine google-chrome
set -e

echo -e "\n\n** UPDATING ZSHENV EDITOR **\n\n"

echo "export EDITOR=nano" >> ~/.zshenv

echo -e "\n\n** INSTALLING SDKMAN (java management) **\n\n"

curl -s "https://get.sdkman.io" | bash
source ~/.sdkman/bin/sdkman-init.sh

echo -e "\n\n** INSTALLING JAVA 17 **\n\n"

sdk install java 17.0.1-tem

echo -en "\007" # beep
echo -e "\n\n** INSTALLING JAVA 11. When prompted to use as default, answer NO. **\n\n"

sdk install java 11.0.15-tem
sdk use java 17.0.1-tem

echo -e "\n\n** GIT CONFIG **\n\n"

git config --global user.name "$fullname"
git config --global user.email "$email"
git config --global core.autocrlf input
ssh-keygen -t rsa -C "$email" -N "" -f "$HOME/.ssh/id_rsa"
ssh-add -K "$HOME/.ssh/id_rsa"

echo -e "\n\n** CONFIGURING POSTGRESQL \n\n**"

brew link postgresql@11 --force
brew pin postgresql@11
brew services start postgresql@11
$CREATE_USER_SCRIPT -s postgres

echo -e "\n\n** STARTING OTHER BREW SERVICES **\n\n"

brew services start nginx
brew services start rabbitmq
brew services start redis

brew services list

echo -e "\n\n** UPDATING /ETC/HOSTS"

echo -en "\007" # beep
echo "WARNING: sudo command(s), password for you mac account might be required!"
echo "127.0.0.1 dev.wrke.io
127.0.0.1 storage.dev.wrke.io
127.0.0.1 login-dev.wrke.io
" | sudo tee -a /etc/hosts

echo -e "\n\n*******************************************\n\n"

echo -e "Add key from below to https://git.wrke.in/-/profile/keys . For convenience, it was copied to your clipboard now.\n\n"
cat $HOME/.ssh/id_rsa.pub | pbcopy
cat $HOME/.ssh/id_rsa.pub
echo -e "\n\n"

echo -en "\007" # beep
read -p "Your browser will now open at https://git.wrke.in/-/profile/keys so that you can add the SSH key. Press Enter to continue." confirm

open https://git.wrke.in/-/profile/keys

while true; do
    read -p "Have you added you SSH key to wrike gitlab (Y/N)? " yn
    case $yn in
        [Yy]* ) break;;
        * ) cat $HOME/.ssh/id_rsa.pub | pbcopy; echo "Please do so! The key should be in your clipboard now, and is written above.";;
    esac
done

echo "Resuming install with this config: "
echo "Project location: $projectpath"

mkdir -p $projectpath

echo -e "\n\n** CREATING WRIKE DIRECTORIES **\n\n"

echo -en "\007" # beep
echo "WARNING: sudo command(s), password for you mac account might be required!"
sudo mkdir -p /tmp/wrike/uploads
sudo mkdir -p /etc/wrike

sudo chown -R ${USER} /etc/wrike
sudo chown -R ${USER} /tmp/wrike/uploads


echo -e "\n\n** EXTRACTING GEOIP DATA **\n\n"

mkdir -p /etc/wrike/geoip/
tar -zxvf GeoLite2-City_20210223.tar.gz -C /etc/wrike/geoip/ GeoLite2-City_20210223/GeoLite2-City.mmdb
mv /etc/wrike/geoip/GeoLite2-City_20210223/GeoLite2-City.mmdb /etc/wrike/geoip/GeoLite2-City.mmdb
rm -r /etc/wrike/geoip/GeoLite2-City_20210223/

pushd $projectpath

echo -e "\n\n** GIT CLONE BACKEND **\n\n"

echo -en "\007" # beep
git clone git@git.wrke.in:backend/backend.git

echo -e "\n\n** GIT HOOKS & PORT MAPPING **\n\n"

pushd backend

./register_client_git_hooks.sh

echo -en "\007" # beep
# this script always returns non-zero result, do not fail whole installation because of it.
set +e
echo "WARNING: sudo command(s), password for you mac account might be required!"
sudo ./setup_port_mapping.sh
set -e

echo -e "\n\n** PGSQL MIGRATION **\n\n"

pushd sql

./wmt setup --all-databases
./wmt status
./wmt up
rm *_dump.json

popd # sql

echo -e "\n\n** JINJA RENDERING **\n\n"

pushd modules/lib/common
mvn jinja:render
popd # modules/lib/common

pushd modules/web-apps/region-server/app-region-server
mvn jinja:render
popd # modules/web-apps/region-server/app-region-server

pushd modules/web-apps/login-server/app-login-server
mvn jinja:render
popd # modules/web-apps/login-server/app-login-server

pushd modules/web-apps/webembedded
mvn jinja:render
popd # modules/web-apps/webembedded

echo -e "\n\n** UPDATING FILE TestSupportAdminCreate.java ** \n\n"

sed -i '' "s/testadminlogin@talogin.com/$email/g" modules/services/support-services/app-support/src/test/java/org/wrike/service/app/TestSupportAdminCreate.java

popd #backend

echo -e "\n\n** CREATING WRIKE_GLOBAL.PROPERTIES **\n\n"

echo "wrike.development.mode=true
wrike.environment=dev
wrike.jms.messaging.enabled=false
wrike.support.account.id=1
wrike.google.apps.marketplace.oauth2.clientId=841249544985-7sb6jco74c9bbsi8fi1l8md399vhi7q4.apps.googleusercontent.com
wrike.google.apps.marketplace.oauth2.clientSecret=pHHQ2FOfGDwroBEolcZ-b4Ip" > /etc/wrike/wrike_global.properties

popd #projectpath

echo -e "\n\n** INSTALLING CUSTOM WRIKE CERT INTO CACERTS **\n\n"

keytool -importcert -file wrke.io_CA.crt -trustcacerts -keystore ~/.sdkman/candidates/java/17.0.1-tem/lib/security/cacerts -alias wrke.io -storepass changeit -noprompt
keytool -importcert -file wrke.io_CA.crt -trustcacerts -keystore ~/.sdkman/candidates/java/11.0.15-tem/lib/security/cacerts -alias wrke.io -storepass changeit -noprompt

keytool -importcert -file $projectpath/backend/conf/nginx/ssl/server.crt -trustcacerts -keystore ~/.sdkman/candidates/java/17.0.1-tem/lib/security/cacerts -alias server -storepass changeit -noprompt
keytool -importcert -file $projectpath/backend/conf/nginx/ssl/server.crt -trustcacerts -keystore ~/.sdkman/candidates/java/11.0.15-tem/lib/security/cacerts -alias server -storepass changeit -noprompt

echo -e "\n\n** ALL AUTOMATIC STEPS ARE DONE. PLEASE, FOLLOW README.MD FOR MANUAL STEPS **\n\n"
