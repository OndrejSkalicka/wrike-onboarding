# Wrike Onboarding Script

## Howto?

### VPN

If you have a VPN already provided by IT, just connect to it.

If not, download VPN client (Cisco AnyConnect). It can be downloaded if you log into https://eu-vpn.team.wrike.com (it will autodetect your OS and provide correct binary). It is recommended to unlock system keychain **before opening the page** as it will prompt you to enter macbook credentials 10+ times otherwise.

Then, either read QA/Prod VPN guru cards below, or just take `wrike-cloud-vpn.xml` file (in this directory) and copy it to `/opt/cisco/anyconnect/profile`, run `Cisco AnyConnect Mobility Client` and connect using your wrike credentials.

Full guru cards for QA and prod VPN:

- QA: https://app.getguru.com/card/i9p69raT/VPN-connection-to-QA-environment-Mac-OS 
- Prod: https://app.getguru.com/card/T5p67r8c/VPN-connection-to-PROD-QA-environments-Mac-OS .

(the QA one is easier to set up. Production will also allow only one device enrolled, eg. if you run this on your new laptop, the old one might lose connectivity.)

### Automatic part

Download this whole repo as a zip file, unzip anywhere (typically in the Downloads folder).

Then, run `./install.sh` and follow instructions. `sudo` is not required (nor allowed). You will be prompted for some input, possibly for root password for commands that require su, to add SSH keys to gitlab etc.

For what is installed, you can check the script, but in general you get (relevant git-dev-onboarding-chapters in brackets):

- homebrew, `[1.1]`
- git, ant, maven, nginx, mc, postgresql@11, rabbitmq, gcc, redis, nano, lima CLI tools `[1.1, 2.4]`,
- slack, jetbrains-toolbox, intellij-idea, zoom GUI tools `[1.1, 1.4]`,
- nano set as default editor,
- SDK man with java 17 and java 11 `[1.1]`,
- config of git (global properties) `[1.3]`,
- config of Postgres (default user, db storage) `[1.5]`,
- SSH keys generated (to be copied to gitlab) `[1.3]`,
- update of /etc/hosts `[2.4]`,
- setup of directories `[2.4]`,
- setup of GeoLite data `[2.4]`,
- git clone of whole backend `[2.1]`,
- ./register_client_git_hooks.sh `[2.1]`,
- ./setup_port_mapping.sh setup (note this has to be called on every reboot) `[2.4]`,
- WMT setup of database `[2.2]`,
- jinja:render calls `[2.4]`,
- update of your email in `TestSupportAdminCreate` `[2.3]`,
- update of wrike_global.properties `[2.3]`,
- import of wrke.io_CA.crt to both java11 and java17 cacerts `[2.4]`.

### Manual part

There are manual steps that were not possible to cover automatically. Follow those in order:

#### Install dev.wrke.io.cer

Install `dev.wrke.io.cer` (located in this directory). Just double-click it and allow apple keychain to import it (to System keychain).

Then, right-click the certificate in keychain, select Get Info, expand section Trust and set to Always trust. NOTE THAT YOU HAVE TO CLOSE THE POPUP USING THE RED BUTTON IN TOP LEFT CORNER, as that is mac's way of saving it.

#### Idea XMS/XMX

Run Idea, click Edit VM options, add:

```
-Xms4096m
-Xmx4096m
```

and restart idea. 

#### Idea license

Idea might need a license assigned to your account. Either start a trial or ask your manager to provide you info on how to get one.

#### Idea Build Heap Size

Open Idea preferences, navigate to `Build, Execution, Deployment | Compiler | Shared build process heap size` and set to at least 800 MB.

If you want super-fast builds, enable also `Compile independent modules in parallel` (and preferably increase `Shared build process heap size` to 2048 MB).

#### Run TestSupportAdminCreate

Test `TestSupportAdminCreate` to create an admin account in your db should already have local changes, just run it. Note that backend will build at this time and it can take about half an hour. You can revert changes to this file afterwards.

#### Run WrikeEmbeddedUnderNginx

Note that you have to add those to Program Arguments of your run configuration:

```
--restartNginx=true --runDirectoryChangesCheck=false
```

#### Happy happy

You should be all set ;)

## Known issues

## TODO

- [x] Add both 'dev.wrke.io.cet' and 'wrke.io_CA.crt' to keychain (system), then mark them both as Always trust.
- [x] Call `mvn jinja:render` in `./backend/modules/web-apps/login-server/app-login-server`.
- [x] Add `backend/conf/nginx/ssl/server.crt` to cacerts.
- [x] Remove xcode tools.
- [X] Prevent sleep via https://stackoverflow.com/a/66201266